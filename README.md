## **Comic Marvel apk
### App Android Clean Architecture

Test Android application using the Marvel API and applying the latest technologies

- **Kotlin**: Programming language used.
- **Retrofit**: Library for Marvel rest api consumption.
- **Marvel API**: API used for data extraction.
- **Coroutines**: Kotlin frendly, design pattern to execute code asynchronously.
- **Koin**: Dependency Injection.
- **Git**: For version control.

> MVI
UDF
LCE
Android Studio
> 
- **MVI** (Model View Intent): Architecture similar to MVVM (ModelViewViewModel) where the main difference is in the communication between the "View" and the "ViewModel". In an MVI architecture, the data necessary for the UI layer is emitted through "LiveData to later be shown to the user".
- **UDF** (Unidirectional Data Flow): Communication between the view and the viewModel follows a UDF pattern. Some of the advantages of implementing this pattern are the following:
  
  - UI control and asynchronous events.
  - Debug in an easy and simple way.
  - This structure maintains 3 key elements:
    - View State: It communicates persistent data states that will be displayed to the user through the UI. For example when retrieving data from an asynchronous call.
    - View Events: Reports user-initiated states. For example a click on a button.
    - View Effects: Communicates non-persistent states in the UI. For example display a "toast" with an indicative message.
- **LCE** (Loading, Content, Error): All application use cases are defined by interfaces. Each interface must indicate that the operation to be executed is of type "suspend". To facilitate error handling and maintain robust communication, LCE is used, it is a pattern that allows indicating at what time the repository call is: Loading, Error or Content:

  - Loading: Allows you to indicate that the call to the service has started.
  - Error: Allows you to indicate that an error has occurred during or at the end of the call to the service. Said error is declared as a hierarchy to be able to differentiate the errors produced by the network (there is no connection, errors returned by the backend, etc.) or an exception.
  - Content: Indicates that the data has been obtained correctly.
- To have a total separation between the domain layer and the remote repository layer, such as Retrofit. Kotlin's inline functions are used to directly return an Lce object, since otherwise Retrofit's own objects would be programmed to the viewModel layer.