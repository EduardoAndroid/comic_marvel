package com.example.comic_marvel.app.base

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

interface BaseActivityRouter {
    fun intent(activity: Context): Intent

    fun launch(activity: Context) = activity.startActivity(intent(activity))
}

interface BaseFragmentRouter {
    fun fragment(): Fragment

    fun add(manager: FragmentManager, containerId: Int, tag: String) =
        manager.beginTransaction().add(containerId, fragment(), tag).commitAllowingStateLoss()


    fun replace(manager: FragmentManager, containerId: Int, tag: String) =
        manager.beginTransaction().replace(containerId, fragment(), tag).commit()


    fun show(manager: FragmentManager, containerId: Int, tag: String) =
        manager.beginTransaction().show(fragment()).commitAllowingStateLoss()

    fun hide(manager: FragmentManager, containerId: Int, tag: String) =
        manager.beginTransaction().hide(fragment()).commitAllowingStateLoss()

    fun remove(manager: FragmentManager, containerId: Int, tag: String) =
        manager.beginTransaction().remove(fragment()).commitAllowingStateLoss()
}