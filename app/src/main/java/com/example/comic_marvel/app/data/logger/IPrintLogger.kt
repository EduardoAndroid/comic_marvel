package com.example.comic_marvel.app.data.logger

interface IPrintLogger {
    fun printError(error: String)
    fun printError(error: Throwable)
}