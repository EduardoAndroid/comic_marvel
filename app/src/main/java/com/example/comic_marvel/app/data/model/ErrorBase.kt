package com.example.comic_marvel.app.data.model

// Error Connection
open class ErrorBase(
    val message: String
) {
    constructor() : this("")
}

// Not successful response
class ErrorWithCode(
    message: String,
    val code: Int,
) : ErrorBase(message) {
    constructor() : this("", 0)
}

// Not connection

class ErrorNotConnection(
    message: String
): ErrorBase(message) {
    constructor(): this("")
}