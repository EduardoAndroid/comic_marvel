package com.example.comic_marvel.app.data.source.retrofit

import com.example.comic_marvel.app.data.model.Comics
import com.example.comic_marvel.app.data.model.Results
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface IApiWebService {

    @GET("/v1/public/comics")
    suspend fun getListComics(): Response<Comics>

    @GET("/v1/public/comics/{comicId}")
    suspend fun getDetailComic(@Path("comicId") comicId: Int): Response<Comics>
}