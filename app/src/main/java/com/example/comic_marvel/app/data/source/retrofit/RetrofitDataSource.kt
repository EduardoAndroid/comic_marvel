package com.example.comic_marvel.app.data.source.retrofit

import com.example.comic_marvel.app.data.logger.IPrintLogger
import com.example.comic_marvel.utils.data.BaseDataSource
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.BuildConfig
import org.koin.java.KoinJavaComponent.inject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.math.BigInteger
import java.security.MessageDigest
import java.util.concurrent.TimeUnit

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}

const val HASH = "hash"
const val APIKEY = "apikey"
const val TS = "ts"
const val TS_VALUE = "1"

class RetrofitDataSource(printLogger: IPrintLogger) {

    private var apiService: IApiWebService? = null
    private var baseData: BaseDataSource? = null

    init {
        this.baseData = BaseDataSource(printLogger)
        initRetrofit()
    }

    private fun initRetrofit() {

        //val authInterceptor: AuthInterceptor by inject(clazz = AuthInterceptor::class.java)
        val networkConnectionInterceptor: NetworkConnectionInterceptor by inject(clazz = NetworkConnectionInterceptor::class.java)

        val gson = GsonBuilder().setLenient().create()
        val logging = createLoggingInterceptor()

        val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val defaultRequest = chain.request()
                val hashSignature = "$TS_VALUE${"9bddb7890d5dbb3d830343b38e812f99f371eb4a"}${"4e38e3a0616ee00391ac9395aea98636"}".md5()//"$TS_VALUE${BuildConfig.PRIVATE_API_KEY_VALUE}${BuildConfig.PUBLIC_API_KEY_VALUE}".md5()
                val defaultHttpUrl = defaultRequest.url
                val httpUrl = defaultHttpUrl.newBuilder()
                    .addQueryParameter(TS, TS_VALUE)
                    .addQueryParameter(APIKEY, "4e38e3a0616ee00391ac9395aea98636")
                    .addQueryParameter(HASH, hashSignature)
                    .build()

                val requestBuilder = defaultRequest.newBuilder().url(httpUrl)

                chain.proceed(requestBuilder.build())
            }
            //.addInterceptor(authInterceptor)
            .addInterceptor(networkConnectionInterceptor)
            .addInterceptor(logging)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl("http://gateway.marvel.com")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        apiService = retrofit.create(IApiWebService::class.java)
    }

    private fun createLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }

    suspend fun getListComics() =
        baseData!!.getResult { apiService!!.getListComics() }

    suspend fun getDetailComic(comicId: Int) =
        baseData?.getResult { apiService?.getDetailComic(comicId) }
}