package com.example.comic_marvel.app.di

import com.example.comic_marvel.app.data.logger.IPrintLogger
import com.example.comic_marvel.app.data.logger.ImplementPrintLogger
import com.example.comic_marvel.app.data.source.retrofit.NetworkConnectionInterceptor
import com.example.comic_marvel.app.data.source.retrofit.RetrofitDataSource
import com.example.comic_marvel.ui.detail_comic.DetailComicViewModel
import com.example.comic_marvel.ui.detail_comic.domain.GetDetailComicService
import com.example.comic_marvel.ui.detail_comic.domain.IGetDetailComic
import com.example.comic_marvel.ui.profile_init.ProfileInitViewModel
import com.example.comic_marvel.ui.profile_init.domain.GetListComicsService
import com.example.comic_marvel.ui.profile_init.domain.IGetListComics
import com.example.comic_marvel.utils.dispatcher.IDispatchersProvider
import com.example.comic_marvel.utils.dispatcher.StandardDispatchers
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.scope.get
import org.koin.dsl.module

@JvmField
val moduleKoin = module {
    single<IDispatchersProvider> { StandardDispatchers() }
    single<IPrintLogger> { ImplementPrintLogger() }
    single { RetrofitDataSource(get()) }
    single { NetworkConnectionInterceptor(androidContext()) }
}

@JvmField
val useCase = module {
    single<IGetListComics> { GetListComicsService(get()) }
    single<IGetDetailComic> { GetDetailComicService(get()) }
}

@JvmField
val viewModel = module {
    viewModel {
        ProfileInitViewModel(get(), get()) }
    viewModel {
        DetailComicViewModel(get(), get()) }
}