package com.example.comic_marvel.ui.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.comic_marvel.R
import com.example.comic_marvel.app.data.model.Comics
import com.example.comic_marvel.app.data.model.Results
import com.example.comic_marvel.databinding.RowComicsBinding

class ComicListAdapter: RecyclerView.Adapter<ComicListAdapter.ViewHolder>() {

    private var comicsList: MutableList<Results> = mutableListOf()
    private var context: Context?= null
    var onClickNext: (comicId: Int) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        this.context = parent.context
        val binding: RowComicsBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context)
            , R.layout.row_comics, parent
            , false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(comic = comicsList[position])
    }

    override fun getItemCount(): Int {
        return comicsList.size
    }

    //standard_large.jpg
    fun setDataListComics(comic: MutableList<Results>?) {
        if (comic != null) {
            this.comicsList = comic
        }
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: RowComicsBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(comic: Results) {
            Log.i("", "")
            var urlImage = comic.thumbnail?.path + "/standard_large.jpg"
            Glide.with(context!!)
                .load(urlImage)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(binding.ivImage)

            binding.root.setOnClickListener {
                comic.id?.let { onClickNext.invoke(it) }
            }
        }
    }
}