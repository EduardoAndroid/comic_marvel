package com.example.comic_marvel.ui.detail_comic

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.comic_marvel.R
import com.example.comic_marvel.app.data.model.Comics
import com.example.comic_marvel.app.data.model.Results
import com.example.comic_marvel.databinding.ActivityDetailComicBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailComicActivity: AppCompatActivity() {

    private lateinit var binding: ActivityDetailComicBinding
    val viewModel: DetailComicViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_comic)

        val comicIc = intent?.getIntExtra(DetailComicRouter.KEY_ID, 0)

        initObservers()
        comicIc?.let { viewModel.getDetailComic(comicId = it) }

    }

    private fun initObservers() {
        viewModel.stateView.observe(this) {
            when (it) {
                is StateViewDetail.GetDetail -> {
                    loadImage(it)
                }
            }
        }
    }

    private fun loadImage(it: StateViewDetail.GetDetail) {
        var urlImage = it.results.data?.results?.get(0)?.thumbnail?.path + "/standard_large." +
                it.results.data?.results?.get(0)?.thumbnail?.extension
        Glide.with(this)
            .load(urlImage)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .into(binding.ivImage)
    }

    sealed class StateViewDetail {
        data class GetDetail(val results: Comics): StateViewDetail()
    }
}