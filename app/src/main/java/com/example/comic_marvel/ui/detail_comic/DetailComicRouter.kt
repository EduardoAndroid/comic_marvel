package com.example.comic_marvel.ui.detail_comic

import android.content.Context
import android.content.Intent
import com.example.comic_marvel.app.base.BaseActivityRouter

class DetailComicRouter: BaseActivityRouter {

    override fun intent(activity: Context): Intent = Intent(activity, DetailComicActivity::class.java)

    override fun launch(activity: Context) {
        activity.startActivity(intent(activity))
    }

    private fun intent(activity: Context, comicId: Int?): Intent {
        val intent = intent(activity)
        intent.putExtra(KEY_ID, comicId)
        return intent
    }

    fun launch(activity: Context, comicId: Int?) {
        activity.startActivity(intent(activity, comicId))
    }

    companion object {
        val KEY_ID = "KEY_ID"
    }
}