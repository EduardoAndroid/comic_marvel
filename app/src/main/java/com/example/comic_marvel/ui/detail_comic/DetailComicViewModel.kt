package com.example.comic_marvel.ui.detail_comic

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.comic_marvel.app.data.model.Comics
import com.example.comic_marvel.ui.detail_comic.domain.IGetDetailComic
import com.example.comic_marvel.utils.data.Lce
import com.example.comic_marvel.utils.dispatcher.IDispatchersProvider
import kotlinx.coroutines.launch

class DetailComicViewModel(
    private val dispatcher: IDispatchersProvider,
    private val useCaseDetailComic: IGetDetailComic
): ViewModel() {

    val stateView: LiveData<DetailComicActivity.StateViewDetail>
        get() = _stateView
    private var _stateView = MutableLiveData<DetailComicActivity.StateViewDetail>()

    fun getDetailComic(comicId: Int) {
        viewModelScope.launch(dispatcher.default) {
            val response = useCaseDetailComic.getDetailComic(comicId = comicId)
            _stateView.postValue(getResponseData(response))
        }
    }

    private fun getResponseData(response: Lce<Comics?>): DetailComicActivity.StateViewDetail? {
        return when(response) {
            is Lce.Content -> {
                response.packet?.let { DetailComicActivity.StateViewDetail.GetDetail(it) }
            }
            is Lce.Error -> {
                null
            }
            is Lce.Loading -> {
                null
            }
        }
    }
}