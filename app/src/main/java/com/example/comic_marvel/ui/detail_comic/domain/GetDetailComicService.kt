package com.example.comic_marvel.ui.detail_comic.domain

import com.example.comic_marvel.app.data.model.Comics
import com.example.comic_marvel.app.data.model.Results
import com.example.comic_marvel.app.data.source.retrofit.RetrofitDataSource
import com.example.comic_marvel.utils.data.Lce

class GetDetailComicService(
    private val dataSource: RetrofitDataSource
): IGetDetailComic {
    override suspend fun getDetailComic(comicId: Int): Lce<Comics?> {
        return dataSource.getDetailComic(comicId)!!
    }
}