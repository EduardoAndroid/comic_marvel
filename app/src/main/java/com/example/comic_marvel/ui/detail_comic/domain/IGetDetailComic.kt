package com.example.comic_marvel.ui.detail_comic.domain

import com.example.comic_marvel.app.data.model.Comics
import com.example.comic_marvel.utils.data.Lce

interface IGetDetailComic {
    suspend fun getDetailComic(comicId: Int): Lce<Comics?>
}