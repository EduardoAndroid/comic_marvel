package com.example.comic_marvel.ui.profile_init

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleObserver
import androidx.recyclerview.widget.GridLayoutManager
import com.example.comic_marvel.R
import com.example.comic_marvel.app.base.BaseActivity
import com.example.comic_marvel.app.data.model.Comics
import com.example.comic_marvel.databinding.ActivityProfileInitBinding
import com.example.comic_marvel.ui.adapters.ComicListAdapter
import com.example.comic_marvel.ui.detail_comic.DetailComicRouter
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class ProfileInitActivity: BaseActivity() {

    private lateinit var binding: ActivityProfileInitBinding
    val viewModel: ProfileInitViewModel by viewModel()
    var adapterComics: ComicListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile_init)

        viewModel.getListComics()
        initRecyclerAdapter()
        initObserver()
    }

    private fun initObserver() {
        viewModel.stateView.observe(this) {
            when (it) {
                is StateViewProfile.DataGetComics -> {
                    adapterComics?.setDataListComics(it.comics.data?.results)
                }
            }
        }
    }

    private fun initRecyclerAdapter() {
        adapterComics = ComicListAdapter()
        binding.rvContentList.apply {
            layoutManager = GridLayoutManager(this.context, 2)
            adapter = adapterComics
            adapterComics?.onClickNext = {
                DetailComicRouter().launch(this@ProfileInitActivity, it)
            }
        }
    }

    sealed class StateViewProfile{
        data class DataGetComics(val comics: Comics) : StateViewProfile()
    }
}