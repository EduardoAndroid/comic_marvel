package com.example.comic_marvel.ui.profile_init

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.comic_marvel.ui.profile_init.domain.IGetListComics
import androidx.lifecycle.viewModelScope
import com.example.comic_marvel.utils.data.Lce
import com.example.comic_marvel.utils.dispatcher.IDispatchersProvider
import kotlinx.coroutines.launch

class ProfileInitViewModel(
    private val dispacher: IDispatchersProvider,
    private val useCaseGetListComics: IGetListComics
) : ViewModel() {

    val stateView: LiveData<ProfileInitActivity.StateViewProfile>
        get() = _stateView
    private val _stateView = MutableLiveData<ProfileInitActivity.StateViewProfile>()

    fun getListComics() {
        viewModelScope.launch(dispacher.default) {
            when(val response = useCaseGetListComics.getListComics()) {
                is Lce.Content -> {
                    response.packet?.let {
                            _stateView.postValue(ProfileInitActivity.StateViewProfile.DataGetComics(it))
                        }
                    }
                is Lce.Error -> {
                    Log.i("", "")
                    /*if(response.error is ErrorWithCode){
                        _stateAwardFree.postValue(AwardShopActivity.StateViewFree.PlayForFree(false))
                    } else if(response.error is ErrorBase){
                        errorHandler.value.logError(TAG, response.error.message)
                    }*/
                }
                is Lce.Loading -> TODO()
            }
        }
    }
}