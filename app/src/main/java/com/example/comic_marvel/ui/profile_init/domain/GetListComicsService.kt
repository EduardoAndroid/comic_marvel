package com.example.comic_marvel.ui.profile_init.domain

import com.example.comic_marvel.app.data.model.Comics
import com.example.comic_marvel.app.data.source.retrofit.RetrofitDataSource
import com.example.comic_marvel.utils.data.Lce

class GetListComicsService(
    private val dataSource: RetrofitDataSource
) : IGetListComics {

    override suspend fun getListComics(): Lce<Comics?> {
        return dataSource.getListComics()
    }
}