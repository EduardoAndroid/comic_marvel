package com.example.comic_marvel.ui.profile_init.domain

import com.example.comic_marvel.app.data.model.Comics
import com.example.comic_marvel.utils.data.Lce

interface IGetListComics {
    suspend fun getListComics(): Lce<Comics?>
}