package com.example.comic_marvel.utils.data

import com.example.comic_marvel.app.data.logger.IPrintLogger
import com.example.comic_marvel.app.data.model.ErrorBase
import com.example.comic_marvel.app.data.model.ErrorNotConnection
import com.example.comic_marvel.app.data.model.ErrorWithCode
import com.example.comic_marvel.app.data.source.retrofit.NoConnectivityException
import retrofit2.Response

class BaseDataSource(
    private val logger: IPrintLogger,
) {
    suspend fun <T> getResult(call: suspend () -> Response<T>?): Lce<T?> {
        try {
            val response = call()
            if (response?.isSuccessful == true) {
                val body = response.body()
                if (body != null) return Lce.Content(body)
            }
            val errorMessage = response?.errorBody()?.charStream()?.readText() ?: ""
            logger.printError(errorMessage)
            return Lce.Error(
                ErrorWithCode(
                    errorMessage,
                    response?.code() ?: 0
                )
            )
        } catch (e: NoConnectivityException) {
            logger.printError(Throwable(e))
            return Lce.Error(ErrorNotConnection(e.message ?: e.toString()))
        } catch (e: Exception) {
            logger.printError(Throwable(e))
            return Lce.Error(ErrorBase(e.message ?: e.toString()))
        }
    }
}