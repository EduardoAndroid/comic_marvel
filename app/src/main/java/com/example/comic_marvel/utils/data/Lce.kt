package com.example.comic_marvel.utils.data

import com.example.comic_marvel.app.data.model.ErrorBase

sealed class Lce<T> {
    data class Loading<T>(val TAG: String) : Lce<T>()
    data class Content<T>(val packet: T) : Lce<T>()
    data class Error<T>(val error: ErrorBase) : Lce<T>()

    override fun toString(): String {
        return when (this) {
            is Loading -> "Loading on $TAG"
            is Content -> "Content[data=$packet]"
            is Error -> "Error=$error]"
        }
    }

    fun getLce() : String {
        return when(this){
            is Loading -> "Loading"
            is Content -> "Content"
            is Error -> "Error"
        }
    }
}